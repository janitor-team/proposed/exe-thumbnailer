Source: exe-thumbnailer
Section: gnome
Priority: optional
Maintainer: Debian Wine Team <debian-wine@lists.debian.org>
Uploaders: James Lu <james@overdrivenetworks.com>
Build-Depends: debhelper-compat (= 12)
Standards-Version: 4.4.0
Homepage: https://github.com/exe-thumbnailer/exe-thumbnailer
Vcs-Git: https://salsa.debian.org/wine-team/exe-thumbnailer.git
Vcs-Browser: https://salsa.debian.org/wine-team/exe-thumbnailer

Package: exe-thumbnailer
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, icoutils, imagemagick, libglib2.0-bin
# msitools provides msiinfo to fetch version tags on .msi files
# wine and liblnk-utils provide winepath and lnkinfo respectively, which are
# used to thumbnail .lnk files
Recommends: liblnk-utils,
            msitools,
            wine,
Enhances: caja, pcmanfm, pcmanfm-qt, tumbler (>= 0.1.92~), nautilus, nemo
Breaks: gnome-exe-thumbnailer (<< 0.10.0-1~)
Replaces: gnome-exe-thumbnailer (<< 0.10.0-1~)
Description: Windows executable (.exe, etc.) thumbnailer for Linux desktops
 exe-thumbnailer is a thumbnailer for Windows executable files
 that shows the embedded icons of .exe, .lnk, .msi, and .dll files when
 available.
 .
 It supports any file manager that uses .thumbnailer entries, including
 Nautilus, Caja, Nemo, Thunar (when tumbler is installed), and PCManFM.

Package: gnome-exe-thumbnailer
Architecture: all
Section: oldlibs
Depends: ${misc:Depends}, exe-thumbnailer
Description: transitional dummy package for exe-thumbnailer
 This is a transitional dummy package for exe-thumbnailer, which can be
 safely removed.
